package controllers

import com.model._
import com.service.ItemsService
import play.api.http.Writeable
import play.api.mvc._
import scala.concurrent.ExecutionContext
import ExecutionContext.Implicits.global

object CalculateBestPrice extends Controller {

  // simple implicit to convert our Items to a simple string for now
  implicit def wItems: Writeable[Items] = Writeable(_.toString.getBytes, Some("application/text"))
  implicit def wListItems: Writeable[List[Items]] = Writeable(_.mkString("\n").getBytes, Some("application/text"))
  implicit def wListPrice: Writeable[List[String]] = Writeable(_.mkString("\n").getBytes, Some("application/text"))

  def createItems(title: String, itemDesc: String,itemQty: Int) = Action.async { request =>
    val body: Option[String] = request.body.asText

    println(request.id)
    //println("request",request)//(request,POST /items?title=hello&itemDesc=Bread&itemQty=4)

    val createdItems = ItemsService.insert(Items(
      -1,
      title,
      body.getOrElse("No body provided"),
      itemDesc,
      itemQty)
    )

    val newItems = createdItems.map(Ok(_))
    //println("NewItems",newItems)
    newItems
  }

  def getItems = Action.async {
    ItemsService.all.map(
      Ok(_)
        .as("application/text")
        .withCookies(new Cookie("play","cookie"))
        .withHeaders(("header1" -> "header1value"))) // specialization if you want to add a date.
  }

  def getPrice = Action.async {
    ItemsService.selectPrice.map(
      Ok(_)
        .as("application/text")
        .withCookies(new Cookie("play","cookie"))
        .withHeaders(("header1" -> "header1value")))
  }

}