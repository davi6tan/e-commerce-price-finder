package controllers

import play.api.mvc._

object Greetings extends Controller {

  def greetings = Action {

    Ok("Greetings !")
  }

}