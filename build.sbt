import sbt._

lazy val commonSettings = Seq(
  version := "0.1.0",
  scalaVersion := "2.11.6"
)
lazy val common = (project in file ("common"))
  .settings(commonSettings: _*)
  .settings(
    name := "common",
    resolvers := DependenciesCommon.resolvers,
    libraryDependencies := DependenciesCommon.backendDeps
  )

addCommandAlias("priceFinder", "; yopp01/run -Dhttp.port=8080  -Dplay.http.router=findbestprice.Routes")

addCommandAlias("testpriceFinder", "; common/test")


import PlayKeys._
lazy val yopp01 = (project in file ("priceFinder"))
  .enablePlugins(PlayScala)
  .dependsOn(common)
  .settings(commonSettings: _*)
  .settings(
    devSettings := Seq("play.server.http.port" -> "8000"),
    name := "e-commerce",
    libraryDependencies := DependenciesYopp.backendDeps
  )

