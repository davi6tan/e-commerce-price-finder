import sbt._

object DependenciesCommon {

  val backendDeps = Seq(
      "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
      "ch.qos.logback" % "logback-classic" % "1.1.2")
  val resolvers = Seq(
    "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/")
}

object DependenciesYopp {

  lazy val playVersion = "2.4.0"

  val backendDeps = Seq (
    "com.typesafe.play" %% "play" % playVersion,
    "com.typesafe.play" %% "play-docs" % playVersion
  )
}

