# A simple API implementation to find the lowest price with various discounts for a collection of items in a purchase cart.




# 1 To run
      
        sbt priceFinder
      
## 1.1 Post items to get discounted price
      
      Example : Post Apple -> 2 , Bread ->1 , Margarine ->3
      
      http://localhost:8080/items?title=PriceFinder&itemDesc=Apple&itemQty=2
      
      http://localhost:8080/items?title=PriceFinder&itemDesc=Bread&itemQty=1
      
      http://localhost:8080/items?title=PriceFinder&itemDesc=Margarine&itemQty=3

      

![post](./common/doc/post.png)



## 1.2 Get all entered items. 
      
      http://localhost:8080/items
      
            
          
![get](./common/doc/get.png)


## 1.3 Get discounted price
       
      http://localhost:8080/price 
       

![getprice](./common/doc/getprice.png)


# 2 To test 
     
     sbt testpriceFinder
     



### 3 Misc - [download starter files](https://bitbucket.org/davi6tan/e-commerce-price-finder/src/eba08f7d86a88233c6e358e9340fee1cf9059736/downloadstarterfiles/?at=master)
     