package com.service

import LowestPriceService._
import com.model._
import org.scalatest._

/**
  * Created by davidtan on 8/20/16.
  */
class PriceFinder extends FlatSpec with Matchers {

  "Apple-> 1" should " be $1.99" in {
    val singleApple = Bundle("Apple Regular",Item("Apple",1))
    findPriceWithComboDiscount(singleApple) should be === 1.99
  }

  "Apple-> 3" should " be $4.14" in {
    val oddNumberApple = Bundle("Apple Odd Number",Item("Apple",3))
    findPriceWithComboDiscount(oddNumberApple) should be === 4.14
  }

  "Apple-> 4 With Discount" should " be $4.30" in {
    val evenNumberApple  = Bundle("Apple Specials",Item("Apple",4))
    findPriceWithComboDiscount(evenNumberApple) should be === 4.3
  }
  "Apple-> 4 Without Discount" should " be $7.96" in {
    val evenNumberApple  = Bundle("Apple Specials",Item("Apple",4))
    findPriceNoDiscount(evenNumberApple) should be === 7.96
  }

  "Banana->2,  Items not listed  " should " be $0.00" in {
    val banana = Bundle("Item Not Listed",Item("Banana",2))
    findPriceWithComboDiscount(banana) should be === 0.00
  }

  "Apple->0,  No item  " should " be $0.00" in {
    val zeroApple= Bundle("No item",Item("Apple",0))
    findPriceWithComboDiscount(zeroApple) should be === 0.00
  }

  "Apple->6, Margarine ->2 and Bread -> 1 Special Price   " should " be $9.57" in {
    val occurrences = Map("Apple" -> 6, "Margarine" -> 2, "Bread" -> 1)
    val emptyBundle = Bundle("Special Combo Price",Item("",0))
    val listOfComboItems = List(("Bread","Margarine"))
    val bundle1 = intoNewBundle(List(),occurrences,emptyBundle,listOfComboItems)

    findPriceWithComboDiscount(bundle1) should be === 9.57
  }

  "Apple->6, Margarine ->2 and Bread -> 1 Without Discount" should " be $16.06" in {
    val occurences = Map("Apple" -> 6, "Margarine" -> 2, "Bread" -> 1)
    val emptyBundle = Bundle("Special Combo Price",Item("",0))
    val listOfComboItems = List(("Bread","Margarine"))
    val bundle1 = intoNewBundle(List(),occurences,emptyBundle,listOfComboItems)
    findPriceNoDiscount(bundle1) should be === 16.06
  }

  "Apple->6, Margarine ->4 and Bread -> 1 Special Price   " should " be $11.57" in {
    val occurrences = Map("Apple" -> 6, "Margarine" -> 4, "Bread" -> 1)
    val emptyBundle = Bundle("Special Combo Price",Item("",0))
    val listOfComboItems = List(("Bread","Margarine"))
    val bundle1 = intoNewBundle(List(),occurrences,emptyBundle,listOfComboItems)

    findPriceWithComboDiscount(bundle1) should be === 11.57
  }
  "Apple->6, Margarine ->4 and Bread -> 1 Without Discount" should " be $18.06" in {
    val occurences = Map("Apple" -> 6, "Margarine" -> 4, "Bread" -> 1)
    val emptyBundle = Bundle("Special Combo Price",Item("",0))
    val listOfComboItems = List(("Bread","Margarine"))
    val bundle1 = intoNewBundle(List(),occurences,emptyBundle,listOfComboItems)
    findPriceNoDiscount(bundle1) should be === 18.06
  }

  "Apple->6, Margarine ->4 and Bread -> 2 Special Price  " should " be $12.69" in {
    val occurrences = Map("Apple" -> 6, "Margarine" -> 4, "Bread" -> 2)
    val emptyBundle = Bundle("Special Combo Price",Item("",0))
    val listOfComboItems = List(("Bread","Margarine"))
    val bundle1 = intoNewBundle(List(),occurrences,emptyBundle,listOfComboItems)
    findPriceWithComboDiscount(bundle1) should be === 12.69
  }


}