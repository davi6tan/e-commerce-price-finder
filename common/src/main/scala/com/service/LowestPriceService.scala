package com.service

import com.model._

/**
  * Created by davidtan on 8/20/16.
  */
object LowestPriceService {
  type Qty = Int
  type Desc = String

  def  intoNewBundle(visited: List[Desc], m1: Map[Desc, Qty], b: Bundle,comboList:List[(String,String)]): Bundle= {
    if (m1.isEmpty) return b

    ////// use all combo ////
    for(el<-comboList) {
      var m2 = m1
      val l = el._1
      val r = el._2

      if (m1.isDefinedAt(l) && m1.isDefinedAt(r) && !visited.contains(l) && !visited.contains(r)) {
        val combo = ComboOrItem(l, m1(l), r, m1(r))
        m2 -= l
        m2 -= r // remove left right
        //println(m2)
        return intoNewBundle(l :: r :: visited, m2, Bundle("This is a combo", b, combo), comboList)
      }
    }

    val item = m1.head._1
    val qty = m1.head._2

    item match {
      case x if visited.contains(item) => intoNewBundle(visited,m1.tail,b,comboList) //skip and go to next
      case y if !visited.contains(item) => intoNewBundle(item:: visited,m1.tail,
        Bundle("Single Item",b,ComboOrItem(item,qty)),comboList)
    }

  }

  def findPriceWithComboDiscount(item: Product): Double = item match {
      case Item("Apple", qty) => {
        //4 -> 4.3 ,,6-> 6.45
        val single_item_price = 1.99
        val double_item_price = 2.15

        qty % 2 * single_item_price + qty / 2 * double_item_price
      }
      case Item("Bread", qty) => qty * 2.12
      case Item("Margarine", qty) => qty * 1.00
      case Item(_, _) => 0.0 // skip not listed

      /// combo 1 buy 1 bread margarine get 1 free
      case Combo("Bread", q1, "Margarine", q2) => {
        //1,2--> 3.12 , 1,3 --> 4.12 --> 1,4 -->5.12 , 2,4->6.24
        val bread = 2.12
        val margarine = 1.00
        val diff = q2 - q1 *2 // 5 - 2

        if (diff > 0) { //to do
          q1 * bread + (q1 + diff) * margarine
        } else {
          q1 * bread + (q2 % 2 + q2 / 2) * margarine

        }
      }
      case Combo(_, _, _, _) => 0.0 // skip not listed

      case Bundle(_, leafs@_*) => leafs.map(findPriceWithComboDiscount).sum
    }

  def findPriceNoDiscount(item: Product): Double = item match {
    case Item("Apple", qty) => qty * 1.99
    case Item("Bread", qty) => qty * 2.12
    case Item("Margarine", qty) => qty * 1.00
    case Item(_, _) => 0.0 // skip not listed
    case Combo("Bread", q1, "Margarine", q2) => q1 * 2.12 + q2 * 1.00
    case Combo(_, _, _, _) => 0.0 // skip not listed
    case Bundle(_, leafs@_*) => leafs.map(findPriceNoDiscount).sum
  }

}
