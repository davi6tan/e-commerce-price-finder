package com.service

import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicLong

import com.model._

import scala.collection.JavaConverters._
import scala.concurrent._
import ExecutionContext.Implicits.global

import scala.concurrent.Future

// Simple service which can store and retrieve Items.
object ItemsService {
  type Qty = Int
  type Desc = String

  private val idGenerator = IdGenerator()

  // An underlying map.
  private val map = new ConcurrentHashMap[Long, Items]().asScala
  private val occ= new ConcurrentHashMap[Desc,Int]().asScala

  def nextItemsId(): Long = idGenerator.next

  def delete(id: Long): Future[Option[Items]] = Future { map.remove(id) }

  def select(id: Long): Future[Option[Items]] = Future { map.get(id) }

  def selectPrice:Future[List[String]] = Future {
    val emptyBundle = Bundle("Special Combo Price",Item("",0))
    val listOfComboItems = List(("Bread","Margarine"))
    val bundle1 = LowestPriceService.intoNewBundle(List(),occ.toMap,emptyBundle,listOfComboItems)
    val lowestPrice = LowestPriceService.findPriceWithComboDiscount(bundle1)
    List(lowestPrice.toString)

  }

  def all: Future[List[Items]] = Future {map.values.toList}

  def insert(u: Items): Future[Items] = Future {
    val ItemsWithId = u.copy(id = nextItemsId())
    map += (ItemsWithId.id -> ItemsWithId)
    if (occ.isDefinedAt(u.desc)) occ(u.desc) += u.qty else occ(u.desc) = u.qty
    ItemsWithId
  }

  def update(id: Long, u: Items): Future[Option[Items]] = Future {
    if (map.contains(id)) {
      map.replace(id, u)
      Some(u)
    } else None
  }

  case class IdGenerator() {
    private val id = new AtomicLong
    def next: Long = id.incrementAndGet
  }
}