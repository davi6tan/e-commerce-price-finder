package com.model

sealed abstract class Product

case class Items(id: Long, title: String, content: String, desc: String, qty:Int)

case class Bundle(var desc: String, items: Product*) extends Product

case class Item(desc: String, qty: Int) extends Product

case class Combo(descA: String, qty1: Int, desc2: String, qty2: Int) extends Product

class ComboOrItem protected(val desc1: String, val qty1: Int, val desc2: String, val qty2: Int)

/**
  *  To add different types of combo beginning with :
  *      1) Bread and Margarine buy 1 bread get margarine 2 for price of one
  */
object ComboOrItem {
  def apply(desc: String, qty: Int) = Item(desc, qty)

  def apply(desc1: String, qty1: Int, desc2: String, qty2: Int) = (desc1, desc2) match {
    case ("Bread", "Margarine") => Combo(desc1, qty1, desc2, qty2)
    case ("Margarine", "Bread") => Combo(desc2, qty2, desc1, qty1)
    case (l, r) => Bundle("NO SPECIALS", Item(desc1, qty1), Item(desc2, qty2))
  }
}


